package jbpersocav;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootCavPersoJbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCavPersoJbApplication.class, args);
	}

}
